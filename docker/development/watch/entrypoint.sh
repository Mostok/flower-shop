#!/bin/sh

set -e

trap "echo 'Shutting down'; kill \$!; exit" SIGINT SIGTERM

if [ -d /var/www/web/node_modules ];
then
npm cache clean --force;
else
npm cache clean --force &&
npm install;
fi

npm run watch;
