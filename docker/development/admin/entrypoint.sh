#!/bin/sh

set -e

trap "echo 'Shutting down'; kill \$!; exit" SIGINT SIGTERM

if [ -f /var/www/web/.env ] && [ -d /app/node_modules ];
then
npm run dev;
elif [ -d /app/node_modules ]
then
cp .env.development .env &&
npm run dev;
else
cp .env.development .env &&
npm i &&
npm run dev;
fi
