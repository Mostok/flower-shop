#!/bin/sh

set -e


if [ -f /var/www/web/.env ] && [ -d /var/www/web/vendor ]
then
php artisan config:cache;
elif [ -d /var/www/web/vendor ]
then
cp .env.development .env &&
php artisan key:generate &&
php artisan jwt:secret &&
php artisan config:cache &&
php artisan storage:link &&
php artisan migrate:fresh --seed;
elif [ -f /var/www/web/.env ]
then
composer i;
else
cp .env.development .env &&
composer i &&
php artisan key:generate &&
php artisan jwt:secret &&
php artisan config:cache &&
php artisan storage:link &&
php artisan migrate:fresh --seed;
fi

# chmod 777 -R /var/www/web/storage &&
# chmod 777 -R /var/www/web/vendor &&
# chmod 777 /var/www/web/.env &&
php-fpm;
