#!/bin/sh

set -e

trap "echo 'Shutting down'; kill \$!; exit" SIGINT SIGTERM

echo "Start the queue..."
php /var/www/web/artisan queue:work
