#!/bin/sh

set -e

trap "echo 'Shutting down'; kill \$!; exit" SIGINT SIGTERM

echo "Start the websockets..."
php /var/www/web/artisan websockets:serve
