#!/bin/bash

set -e


if [ -f /var/www/web/.env ] && [ -d /var/www/web/vendor ]
then
php artisan config:cache;
elif [ -d /var/www/web/vendor ]
then
cp .env.production .env &&
php artisan key:generate &&
php artisan jwt:secret &&
php artisan config:cache &&
php artisan storage:link &&
php artisan migrate;
elif [ -f /var/www/web/.env ]
then
composer i;
else
cp .env.production .env &&
composer i &&
php artisan key:generate &&
php artisan jwt:secret &&
php artisan config:cache &&
php artisan storage:link &&
php artisan migrate;
fi

# chmod 777 -R /var/www/web/storage &&
# chmod 777 -R /var/www/web/vendor &&
# chmod 777 /var/www/web/.env &&
php-fpm;