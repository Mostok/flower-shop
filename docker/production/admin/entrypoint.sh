#!/usr/bin/env bash
set -e

trap "echo 'Shutting down'; kill \$!; exit" SIGINT SIGTERM

if [ -f /var/www/web/.env ] && [ -d /app/node_modules ];
then
npm run build-watch;
elif [ -d /app/node_modules ]
then
cp .env.development .env &&
npm run build-watch;
else
cp .env.development .env &&
npm i &&
npm run build-watch;
fi