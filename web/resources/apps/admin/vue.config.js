module.exports = {
    // devServer: {
    //     proxy: 'http://127.0.0.1:8098'
    // },
    'transpileDependencies': [

        'vuetify'
    ],
    // output built static files to Laravel's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: '../../../public/assets/admin',

    publicPath: '/admin',
    assetsDir: '../../assets/admin/',
    // modify the location of the generated HTML file.
    indexPath: '../../../resources/views/admin.blade.php',
};