import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import plugins from "./plugins";

import vuetify from "./plugins/vuetify";
import websockets from "./plugins/websockets";
import axios from "./plugins/axios";
import auth from "./plugins/auth";
import "roboto-fontface/css/roboto/roboto-fontface.css";
import "@mdi/font/css/materialdesignicons.css";

import "vue-select/dist/vue-select.css";

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    vuetify,
    axios,
    auth,
    plugins,
    websockets,
    render: (h) => h(App),
}).$mount("#app");