import Vue from "vue";

import axios from "axios";

function notifyError() {
  Vue.notify({
    group: "foo",
    type: "error",
    duraptation: 10000,
    speed: 1000,
    title: "<h2>Ошибка загрузки данных</h2>",
    text:
      "<b>Не удалось загрузить данные пользователей, пожалуйста повторите попытку позже, или обратитесь в тех поддержку</b>",
  });
}

const userWebStore = {
  namespaced: true,

  state: {
    users: [],
  },

  mutations: {
    ADD_USERS(state, data) {
      state.users = data;
    },

    DEL_USER(state, user) {
      state.users.splice(state.users.indexOf(user), 1);
    },

    UPD_USER(state, data) {
      Object.assign(state.users[data.index], {
        ...data.res.user,
      });
    },

    NEW_USER(state, user) {
      state.users.push(user);
    },
  },

  getters: {
    webUsers: ({ users }) => {
      return users;
    },
  },

  actions: {
    fetchUsers({ commit }) {
      axios
        .get("/users/web/list")
        .then((res) => {
          commit("ADD_USERS", res.data);
        })
        .catch((error) => {
          notifyError(error);
        });
    },

    deleteUser({ commit }, user) {
      console.log(user);
      commit("DEL_USER", user);
      axios
        .delete(`users/web/${user.id}`)
        .then(() => {
          commit("DEL_USER", user);
        })
        .catch((error) => {
          notifyError(error);
        });
    },

    updateUser({ commit }, data) {
      axios
        .post(`users/web/${data.user.id}/update`, data.user)
        .then((res) => {
          commit("UPD_USER", { res: res.data, index: data.index });
        })
        .catch((error) => {
          notifyError(error);
        });
    },

    storeUser({ commit }, user) {
      console.log(user);
      axios
        .post(`/users/admin/store`, user)
        .then((res) => {
          commit("NEW_USER", res.data.user);
        })
        .catch((error) => {
          notifyError(error);
        });
    },
  },
};
export default userWebStore;
