import Vue from "vue";

const authStore = {
  namespaced: true,

  actions: {
    fetch(data) {
      return Vue.auth.fetch(data);
    },

    refresh(data) {
      return Vue.auth.refresh(data);
    },

    login(ctx, payload) {
      return Vue.auth.login({
        data: payload.body,
        remember: true,
        staySignedIn: payload.staySignedIn,
        redirect: { name: "Dashboard" },
      });
    },

    logout() {
      return Vue.auth.logout();
    },
  },

  getters: {
    user() {
      return Vue.auth.user();
    },
  },
};

export default authStore;
