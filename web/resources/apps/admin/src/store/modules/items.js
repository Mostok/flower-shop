import Vue from "vue";

import axios from "axios";

function notifyError() {
  Vue.notify({
    group: "foo",
    type: "error",
    duraptation: 10000,
    speed: 1000,
    title: "<h2>Ошибка загрузки данных</h2>",
    text:
      "<b>Не удалось загрузить товары, пожалуйста повторите попытку позже, или обратитесь в тех поддержку</b>",
  });
}

const itemsStore = {
  namespaced: true,

  state: {
    items: [],
  },

  mutations: {
    ADD_ITEMS(state, data) {
      state.items = data;
    },
    DEL_ITEM(state, item) {
      state.items.splice(state.items.indexOf(item), 1);
    },
    UPD_ITEM(state, data) {
      Object.assign(state.items[data.index], {
        ...data.res.item,
      });
    },
    NEW_ITEM(state, item) {
      state.items.push(item);
    },
    SET_ACTIVE(state, data) {
      Object.assign(state.items[data.index], {
        active: 1,
      });
    },
  },

  getters: {
    items: ({ items }) => {
      return items;
    },
    selectItems: ({ items }) => {
      return items.map((item) => {
        let mutated = new Object();
        mutated.text = item.name;
        mutated.value = item.id;
        return mutated;
      });
    },
  },
  actions: {
    fetchItems({ commit }) {
      axios
        .get("/items/list")
        .then((res) => {
          commit("ADD_ITEMS", res.data);
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    deleteItem({ commit }, item) {
      axios
        .delete(`items/${item.id}`)
        .then(() => {
          commit("DEL_ITEM", item);
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    updateItem({ commit }, data) {
      data.item.categories = data.item.categories.map(
        (category) => category.id,
      );
      axios
        .post(`items/${data.item.id}/update`, data.item)
        .then((res) => {
          commit("UPD_ITEM", { res: res.data, index: data.index });
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    storeItem({ commit }, { item }) {
      item.categories = item.categories.map((category) => category.id);
      axios
        .post(`/items/store`, item)
        .then((res) => {
          commit("NEW_ITEM", res.data.item);
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    setActive({ commit }, data) {
      axios
        .get(`items/${data.id}/active`)
        .then(() => {
          commit("SET_ACTIVE", data);
        })
        .catch((err) => {
          notifyError(err);
        });
    },
  },
};

export default itemsStore;
