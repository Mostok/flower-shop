import Vue from "vue";

import axios from "axios";

function notifyError() {
  Vue.notify({
    group: "foo",
    type: "error",
    duraptation: 10000,
    speed: 1000,
    title: "<h2>Ошибка загрузки данных</h2>",
    text:
      "<b>Не удалось загрузить заказы, пожалуйста повторите попытку позже, или обратитесь в тех поддержку</b>",
  });
}

const statusesConvert = [
  { id: 1, text: "Новый" },
  { id: 2, text: "Отменен" },
  { id: 3, text: "Ожидает оплаты" },
  { id: 4, text: "Оплачен" },
  { id: 5, text: "В работе" },
  { id: 6, text: "Завершен" },
];

const ordersStore = {
  namespaced: true,

  state: {
    orders: [],
  },

  mutations: {
    ADD_ORDERS(state, data) {
      state.orders = data;
    },

    DEL_ORDER(state, order) {
      state.orders.splice(state.orders.indexOf(order), 1);
    },

    UPD_ORDER(state, data) {
      Object.assign(state.orders[data.index], {
        ...data.res.order,
      });
    },

    NEW_ORDER(state, order) {
      state.orders.push(order);
    },

    CHANGE_STATUS(state, data) {
      Object.assign(state.orders[data.index], {
        status: data.order.status,
      });
    },
  },

  getters: {
    orders: ({ orders }) => {
      const ordersArray = orders.map((order) => {
        const newOrder = JSON.parse(JSON.stringify(order));
        newOrder.status = {
          id: order.status.id,
          status: order.status.status,
          text: statusesConvert.find((x) => x.id === order.status.id).text,
        };
        return newOrder;
      });
      return ordersArray;
    },
  },

  actions: {
    fetchOrders({ commit }) {
      axios
        .get("/orders/list")
        .then((res) => {
          commit("ADD_ORDERS", res.data);
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    deleteOrder({ commit }, order) {
      axios
        .delete(`orders/${order.id}`)
        .then(() => {
          commit("DEL_ORDER", order);
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    updateOrder({ commit }, data) {
      axios
        .post(`orders/${data.order.id}/update`, data.order)
        .then((res) => {
          commit("UPD_ORDER", { res: res.data, index: data.index });
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    async storeOrder({ commit }, order) {
      if (order.user.phone !== "") {
        let userData = {
          client: order.user.client,
          name: order.user.name,
          phone: order.user.phone,
        };
        await axios
          .post("/users/web/store", userData)
          .then((res) => {
            if (res.data.success) {
              order.user.client = res.data.result.id;
              order.user.name = res.data.result.name;
              order.user.phone = res.data.result.phone;
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      await axios
        .get(`orders/cart/${order.user.client}`)
        .then((res) => {
          order.cart.id = res.data.result.id;
        })
        .catch((error) => {
          console.log(error);
        });

      let itemsData = {
        cart_id: order.cart.id,
        items: order.cart.items,
      };

      await axios
        .post(`orders/items`, itemsData)
        .then(() => {})
        .catch((error) => {
          console.log(error);
        });

      let deleveryData = {
        client_id: order.user.client,
        delivery: order.delivery,
      };
      await axios
        .post(`orders/delivery`, deleveryData)
        .then((res) => {
          order.delivery.id = res.data.id;
        })
        .catch((error) => {
          console.log(error);
        });

      let orderData = {
        delivery_id: order.delivery.id,
        cart_id: order.cart.id,
      };
      await axios
        .post(`/orders/store`, orderData)
        .then((res) => {
          commit("NEW_ORDER", res.data.order);
        })
        .catch((err) => {
          notifyError(err);
        });
    },

    changeOrderStatus({ commit }, data) {
      axios
        .post("/orders/change/status", {
          order_id: data.order.id,
          status_id: data.order.status.id,
        })
        .then(() => {
          commit("CHANGE_STATUS", data);
        })
        .catch((err) => {
          notifyError(err);
        });
    },
  },
};

export default ordersStore;
