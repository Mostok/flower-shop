import Vue from "vue";

import axios from "axios";

function notifyError() {
  Vue.notify({
    group: "foo",
    type: "error",
    duraptation: 10000,
    speed: 1000,
    title: "<h2>Ошибка загрузки данных</h2>",
    text:
      "<b>Не удалось загрузить товары, пожалуйста повторите попытку позже, или обратитесь в тех поддержку</b>",
  });
}

const categoriesStore = {
  namespaced: true,

  state: {
    categories: [],
  },

  mutations: {
    ADD_CATEGORIES(state, data) {
      state.categories = data;
    },
    DEL_CATEGORY(state, category) {
      state.categories.splice(state.categories.indexOf(category), 1);
    },
    UPD_CATEGORY(state, data) {
      Object.assign(state.categories[data.index], {
        ...data.res.category,
      });
    },
    NEW_CATEGORY(state, category) {
      state.categories.push(category);
    },
  },

  getters: {
    categories: ({ categories }) => {
      return categories;
    },
    filteredCategories: ({ categories }) => {
      return categories.map((category) => {
        let mutated = new Object();
        mutated.text = category.name;
        mutated.value = category.id;
        return mutated;
      });
    },
  },

  actions: {
    fetchCategories({ commit }) {
      axios
        .get("/categories/list")
        .then((res) => {
          commit("ADD_CATEGORIES", res.data);
        })
        .catch((err) => {
          notifyError(err);
        });
    },
    deleteCategory({ commit }, category) {
      axios
        .delete(`categories/${category.id}`)
        .then(() => {
          commit("DEL_CATEGORY", category);
        })
        .catch((err) => {
          notifyError(err);
        });
    },
    updateCategory({ commit }, data) {
      axios
        .post(`categories/${data.category.id}/update`, data.category)
        .then((res) => {
          commit("UPD_CATEGORY", { res: res.data, index: data.index });
        })
        .catch((err) => {
          notifyError(err);
        });
    },
    storeCategory({ commit }, { category }) {
      axios
        .post(`/categories/store`, category)
        .then((res) => {
          commit("NEW_CATEGORY", res.data.category);
        })
        .catch((err) => {
          notifyError(err);
        });
    },
  },
};

export default categoriesStore;
