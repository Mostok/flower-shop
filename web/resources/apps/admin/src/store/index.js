import Vue from "vue";
import Vuex from "vuex";

import auth from "./modules/auth";
import items from "./modules/items";
import categories from "./modules/categories";
import orders from "./modules/orders";
import usersWeb from "./modules/user-web";
import usersAdmin from "./modules/user-admin";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    items,
    categories,
    orders,
    usersWeb,
    usersAdmin,
  },
});
