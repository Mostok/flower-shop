import Vue from "vue";

import auth from "@websanova/vue-auth";
import authBearer from "@websanova/vue-auth/drivers/auth/bearer.js";
import httpAxios from "@websanova/vue-auth/drivers/http/axios.1.x.js";
import routerVueRouter from "@websanova/vue-auth/drivers/router/vue-router.2.x.js";

Vue.use(auth, {
  auth: authBearer,
  http: httpAxios,
  router: routerVueRouter,
  rolesKey: "roles",
  refreshData: {
    url: "/auth/refresh",
    method: "GET",
    enabled: true,
    interval: 100,
  },
  parseUserData: function(res) {
    return res;
  },
  logoutData: {
    url: "auth/logout",
    method: "POST",
    redirect: "/login",
    makeRequest: false,
  },
});
