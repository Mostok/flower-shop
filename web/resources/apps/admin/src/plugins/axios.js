import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";

axios.defaults.baseURL = process.env.VUE_APP_API_URL;
axios.defaults.publicURL = process.env.VUE_APP_PUBLIC_URL;

Vue.use(VueAxios, axios)