import Echo from 'laravel-echo'
window.Pusher = require('pusher-js')

window.Echo = new Echo({
    broadcaster: 'pusher',
    key: process.env.VUE_APP_PUSHER_APP_KEY,
    wsHost: window.location.hostname,
    wsPort: process.env.VUE_APP_WEBSOCKETS_PORT,
    wssPort: process.env.VUE_APP_WEBSOCKETS_PORT_WSS,
    enabledTransports: process.env.VUE_APP_PUSHER_ENABLED_TRANSPORTS.split(','),
    enableStats: false,
    cluster: process.env.MIX_PUSHER_APP_CLUSTER,
})