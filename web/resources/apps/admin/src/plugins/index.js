import Vue from "vue";

import Notifications from "vue-notification";
Vue.use(Notifications);

import vSelect from "vue-select";

Vue.component("v-select", vSelect);
