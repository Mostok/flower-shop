import Vue from "vue";
import VueRouter from "vue-router";

import Admin from "../views/admin.vue";
import Login from "../views/login.vue";
import Categories from "../views/categories.vue";
import Items from "../views/items.vue";
import UsersWeb from "../views/users-web.vue";
import UsersAdmin from "../views/users-admin.vue";
import Account from "../views/account.vue";
import Dashboard from "../views/dashboard.vue";
import Order from "../views/orders.vue";
import Promo from "../views/promo.vue";
import Extras from "../views/extras.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "",
    component: Admin,
    meta: {
      auth: true,
    },
    children: [
      { path: "categories", component: Categories, name: "Categories" },
      { path: "items", component: Items, name: "Items" },
      { path: "extras", component: Extras, name: "Extras" },
      { path: "account", component: Account, name: "Account" },
      { path: "order", component: Order, name: "Order" },
      { path: "promo", component: Promo, name: "Promo" },
      {
        path: "users/web",
        component: UsersWeb,
        name: "UsersWeb",
        meta: {
          auth: {
            roles: "superadmin",
          },
        },
      },
      {
        path: "users/admin",
        component: UsersAdmin,
        name: "UsersAdmin",
        meta: {
          auth: {
            roles: "superadmin",
          },
        },
      },
      { path: "", component: Dashboard, name: "Dashboard" },
    ],
  },
  {
    path: "/login",
    component: Login,
    name: "Login",
    meta: {
      auth: false,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

Vue.router = router;
export default router;
