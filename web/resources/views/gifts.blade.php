@extends('layout')
@section('title', 'Aroma-Flowers - Подарки в Таганроге')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Подарки ручной работы в Таганроге! Выбери и оформи прямо на сайте. Бесплатно доставим Ваш подарок по городу Таганрог. Постоянным клиентам скидки!" />
<meta name="keywords" content="подарки Таганрог, купить подарки в Таганроге, подарки с доставкой Таганрог, магазин подарков в Таганроге, купить подарки онлайн, заказать подарки, подарки ручной работы" />
<link rel="canonical" href="https://aroma-flowers.ru/gifts">
@endsection
@section('facebook_meta')
<meta property="og:url" content="https://aroma-flowers.ru/gifts" />
<meta property="og:title" content="Подарки ручной работы в Таганроге! Выбери и оформи прямо на сайте. Бесплатно доставим Ваш подарок по городу Таганрог. Постоянным клиентам скидки!" />
<meta property="og:description" content="Заказывать онлайн удобно. Подарки в Таганроге! Выбери и оформи заказ прямо на сайте!" />
<meta property="og:type" content="website" />
<meta property="og:image" content="asset('images/og2.png')" />
@endsection
@section('content')
<catalogue :load="{{ $items }}" :types="'gifts'"></catalogue>
@endsection
