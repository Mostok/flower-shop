<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--metatextblock-->
    @yield('meta')
    @yield('facebook_meta')
    <!--/metatextblock-->
</head>
<body>
    <div class="wrapper" id="app">
        @include('partials/_header')
        <div class="wrapper">
            <div class="slider">
                <view-slider></view-slider>
            </div>
            <div class="content">
                @section('content')
                @show
            </div>

        </div>
        @include('partials/_footer')

    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
