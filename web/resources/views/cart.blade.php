@extends('layout')
@section('title', 'Aroma-flowers.ru - Корзина')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Оформить заказ онлайн в Таганроге" />
<meta name="keywords" content="цветы купить онлайн в Таганроге, купить цветы онлайн, оформить заказ цветы онайлн" />
<link rel="canonical" href="https://aroma-flowers.ru/cart">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title mt-2">
            <h1>Корзина</h1>
        </div>

    </div>
</div>

<cart-component></cart-component>
@endsection
