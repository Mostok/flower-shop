@extends('layout')
@section('title', 'Aroma-flowers.ru - Цветочный аромат в каждый дом')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Купить букет в Таганроге! Выбери и оформи заказ прямо на сайте! Бесплатная доставка по городу Таганрог" />
<meta name="keywords" content="цветы Таганрог, купить цветы в Таганроге, цветы с доставкой Таганрог, магазин цветов в Таганроге, купить цветы онлайн, заказать цветы" />
<link rel="canonical" href="https://aroma-flowers.ru/">
@endsection
@section('facebook_meta')
<meta property="og:url" content="https://aroma-flowers.ru" />
<meta property="og:title" content="Aroma-flowers.ru | Бонусы при заказе на сайте" />
<meta property="og:description" content="Заказывать онлайн удобно. Бизнес-букеты в Таганроге! Выбери и оформи заказ прямо на сайте!" />
<meta property="og:type" content="website" />
<meta property="og:image" content="asset('images/og2.png')" />
@endsection
@section('content')
<catalogue :load="{{ $items }}" :types="'flowers'"></catalogue>
<div class="container">
    <div class="col-lg-12 text-center font-secondary regular middle pt-4 pb-4">
        Посмотрите какие интересные подарки есть в нашем магазине!
    </div>
    <div class="row">
        @foreach($recommends as $recommend)
        @if (count($recommend->photos) > 0)
        <div class="p-2 col-lg-4 mt-4 text-center">
            <div class="item-box">
                <div class="price-box">{{ $recommend->price }} &#8381;</div>

                <div class="img-hover-zoom-brightness">
                    <img class="img w-75" src="{{ $recommend->photos[0]->path }}" alt="{{ $recommend->name }} 'купить в Таганроге'" />
                </div>
                <h3>{{ $recommend->name }}</h3>
                <a href="/items/{{ $recommend->id }}" title="Подробнее о товаре">
                    <button type="button" class="order-button rounded p-2 mt-2">Посмотреть</button>
                </a>
            </div>
        </div>
        @endif
        @endforeach
    </div>
</div>

@endsection
