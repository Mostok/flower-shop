<div class="text-center header p-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 pt-5 pr-2">
                <p>
                    <big>
                        <u><a class="text-white" href="tel:+79188965442">+7-918-896-54-42</a></u>
                    </big>
                </p>
                <p>
                    <small>
                        <u>г. Таганрог</u>
                    </small>
                </p>
            </div>
            <div class="col-lg-6 mt-4">
                <info-message></info-message>
                <a href="/">
                    <img class="w-25 col-lg-4" src="{{ asset('images/CircledLogo.svg') }}" alt="Aroma-Flowers.ru - Логотип круглый">
                    <img class="w-100 col-lg-8" src="{{ asset('images/AromaLogo_4.svg') }}" alt="Aroma-Flowers.ru - Название">
                </a>
            </div>
            <div class="col-lg-3 pt-5 pr-2">
                @if(Auth::check())
                <a href="/user" class="p-2 decoration-none font-secondary">
                    {{ Auth::user()->name }}
                    @else
                    <a href="/login" class="p-2 decoration-none font-secondary">
                        Вход
                        @endif
                        <img class="quick-button" src="{{ asset('images/profile.svg') }}" alt="Вход">
                    </a>

                    <a href="/cart" class="p-2 decoration-none font-secondary">
                        Корзина
                        <img class="quick-button" src="{{ asset('images/cart.svg') }}" alt="Корзина">
                    </a>
            </div>
            <div class="col-lg-12">
                <nav>
                    <a href="/" title="Каталог букетов">Каталог</a>
                    <a href="/delivery" title="Доставка букетов в Таганроге">Доставка</a>
                    <a href="/gifts" title="Подарки ручной работы в Таганроге">Подарки</a>
                    <a href="/greeting-cards" title="Уникальные открытки в Таганроге">Открытки</a>
                    {{-- <a href="#">Растения</a> --}}
                    {{-- <a href="#">Стать партнером</a> --}}
                    <a href="/about" title="Кто мы такие">О нас</a>
                </nav>
            </div>
        </div>
    </div>
</div>
