@extends('layout')
@section('title', 'Aroma-flowers.ru - Открытки в Таганроге')
@section('meta')
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta name="description" content="Открытки ручной работы в Таганроге! Выбери и оформи прямо на сайте. Бесплатная доставка по городу Таганрог, постоянным клиентам скидки." />
<meta name="keywords" content="открытки Таганрог, купить подарки в Таганроге, подарки с доставкой Таганрог, магазин подарков в Таганроге, купить подарки онлайн, заказать подарок" />
<link rel="canonical" href="https://aroma-flowers.ru/greeting-cards">
@endsection
@section('facebook_meta')
<meta property="og:url" content="https://aroma-flowers.ru/greeting-cards" />
<meta property="og:title" content="Aroma-flowers.ru - Открытки в Таганроге" />
<meta property="og:description" content="Открытки ручной работы в Таганроге! Выбери и оформи прямо на сайте. Бесплатная доставка по городу Таганрог, постоянным клиентам скидки." />
<meta property="og:type" content="website" />
<meta property="og:image" content="asset('images/og2.png')" />
@endsection
@section('content')
<catalogue :load="{{ $items }}" :types="'greeting'"></catalogue>
@endsection
