@extends('layout')
@section('title', 'Aroma-Flowers - Личный кабинет')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="page-title mt-2">
            <h1>Личный Кабинет</h1>
        </div>

    </div>
</div>

<cabin-component :orders="{{ $orders }}" :user="{{ $user }}" :state="{{ $level_state }}"></cabin-component>
@endsection

