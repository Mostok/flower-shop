<html>
<head>
    <title>Aroma-flowers.ru - О нас</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!--metatextblock-->
    <meta name="description" content="Цветочная мастерская онлайн. Потрясающие букеты в Таганроге! Выбери и оформи заказ прямо на сайте! Бесплатная доставка по городу Таганрог" />
    <meta name="keywords" content="цветы Таганрог, купить цветы в Таганроге, цветы с доставкой Таганрог, магазин цветов в Таганроге, купить цветы онлайн, заказать цветы" />
    <link rel="canonical" href="https://aroma-flowers.ru/about">
    <meta property="og:url" content="https://aroma-flowers.ru/about" />
    <meta property="og:title" content="Aroma-flowers.ru - О нас" />
    <meta property="og:description" content="Цветочная мастерская онлайн. Потрясающие букеты в Таганроге! Выбери и оформи заказ прямо на сайте! Бесплатная доставка по городу Таганрог" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="asset('images/og2.png')" /> 

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
</head>
<body>
    <div class="wrapper" id="app">
        @include('partials/_header')
        <div class="wrapper">
            <div class="container-fluid mt-4">
                <div class="row flex-center about-page">
                    <div class="col-lg-6 p-4">
                        <p class="text-center">
                            <b>
                                Добро пожаловать в нашу мастерскую! Если ты сюда зашел, значит тебе интересно, кто мы такие и что в наших букетах такого особенного?
                            </b>
                        </p>
                        <img class="col-lg-6 p-4 float-right rounded" src="{{ asset('images/about.jpg')}}" alt="Цветочная мастерская Аромат">
                        <p class="text-justify">Наши флористы постоянно следят за качеством срезанных цветов и бережно собирают для Вас самые необычные композиции, а наши курьеры спешат вам их доставить, чтобы принести Аромат в каждый дом.
                            В любую погоду, по любому поводу наши букеты окружат Вас запахом весны, цветущего лета и утреннего поля.
                            Они поднимут настроение, придадут решительности, заставят улыбнуться в дождливый день.</p>
                        <p class="text-justify">Чтобы Вам было проще сделать заказ мы разработали собственную платформу - <a href="https://aroma-flowers.ru">Aroma-flowers.ru</a></p>
                        <p class="text-justify">Так же у нас можно заказать оригинальные <a href="https://aroma-flowers.ru/gifts">подарки ручной работы</a>, а так же <a href="https://aroma-flowers.ru/greeting-cards">открытки</a>, нарисованные нашими мастерами.</p>
                        <p class="text-justify">На нашей платформе действует бесплатная <a href="https://aroma-flowers.ru/delivery">доставка букетов</a> при заказе от 1200 руб.</p>
                        <p class="text-justify">Сделайте свой первый заказ, чтобы <a href="https://aroma-flowers.ru/login">зарегистрироваться</a> и получить массу преимуществ постоянного Клиента.</p>
                        <p class="text-justify">Индивидуальные рекомендации на основе Ваших предпочтений, бонусы и скидки за каждый заказанный букет - лишь малая часть преимуществ нашей платформы.</p>
                        <p class="text-justify">И не забывайте - Вы всегда можете придти к нам в мастерскую по адресу:
                            <b>
                                г.Таганрог, пер. Красногвардейский 6Б
                            </b>
                            и наши флористы подберут индивидуальный букет.</p>
                        <p>До встречи в мастерской!</p>
                    </div>
                </div>
            </div>
        </div>
        @include('partials/_footer')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
