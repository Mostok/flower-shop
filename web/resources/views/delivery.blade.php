<html>
<head>
    <title>Aroma-flowers.ru - Доставка</title>
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}" type="image/png">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!--metatextblock-->
    <meta name="description" content="Цветочная мастерская онлайн. Потрясающие букеты в Таганроге! Выбери и оформи заказ прямо на сайте! Бесплатная доставка по городу Таганрог" />
    <meta name="keywords" content="доставка цветов в Таганроге, купить цветы в Таганроге, цветы с доставкой Таганрог, магазин цветов в Таганроге, купить цветы онлайн, заказать цветы онлайн" />
    <link rel="canonical" href="https://aroma-flowers.ru/delivery">
    <meta property="og:url" content="https://aroma-flowers.ru/delivery" />
    <meta property="og:title" content="Aroma-flowers.ru - Доставка" />
    <meta property="og:description" content="Заказывать онлайн удобно. Бизнес-букеты в Таганоге! Выбери и оформи заказ прямо на сайте!" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="asset('images/og2.png')" />

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
</head>
<body>
    <div class="wrapper" id="app">
        @include('partials/_header')
        <div class="wrapper">
            <div class="container-fluid mt-4">
                <div class="row flex-center about-page">
                    <div class="col-lg-6 p-4">
                        <p class="text-center">
                            <b>
                                Способ доставки
                            </b>
                        </p>
                        <p>
                            Вы можете выбрать один из двух способов доставки:
                        </p>
                        <p><u>
                                <b>
                                    Доставка по предварительному звонку получателю
                                </b>
                            </u>
                        </p>
                        <p class="font-weight-light">
                            Мы созваниваемся с получателем перед доставкой и уточняем время и адрес. При оформлении заказа вы можете указать, чтобы мы сообщали или не сообщали получателю о том, что это доставка цветов.
                        </p>
                        <p>
                            <u>
                                <b>
                                    Доставка без звонка в указанный промежуток времени
                                </b>
                            </u>
                        </p>
                        <p>
                            Мы не созваниваемся с получателем и сохраняем эффект сюрприза во время доставки.
                        </p>
                        <p>
                            <u>
                                <b>
                                    Доставка на сумму от 1200 руб осуществляется бесплатно
                                </b>
                            </u>
                        </p>
                        <p>
                            Цена доставки на меньшую сумму оговаривается отдельно и зависит от пожеланий Клиента.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @include('partials/_footer')
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
