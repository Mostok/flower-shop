<?php

namespace App\Listeners;

use App\Events\OrderCreated;

class SendCreatedNotificationVK
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(OrderCreated $event)
    {
        // VK userIDs
        $this->managers = ['155604988'];
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        foreach ($this->managers as $manager) {
            $this->sendNotification($manager, $event->order);
        }
    }

    public function sendNotification($manager, $order)
    {
        $success = false;
        $user = $order->cart()->first()->user;
        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', "https://api.vk.com/method/messages.send", [
            'form_params' => array(
                'v' => '5.52',
                'access_token' => config('app.vk_api'),
                'message' => 'Поступил новый заказ от клиента ' . $user->name . " " . $user->phone . ". Подробнее: https://control.aroma-flowers.ru/order",
                'user_id' => $manager,
            ),
        ]);

        $contents = json_decode($response->getBody()->getContents());

        if(!isset($contents->error)) {
            $success = true;
        }

        return $success;
    }
}
