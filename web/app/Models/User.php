<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'temp', 'cart'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the deliveries for concrete User.
     */
    public function deliveries()
    {
        return $this->hasMany('App\Models\Delivery');
    }

    /**
     * Get the carts for concrete User.
     */
    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    /**
     * Get the Wish List for concrete User.
     */
    public function wish_list()
    {
        return $this->hasOne('App\Models\WishList');
    }

    /**
     * Get the Bonus for concrete User.
     */
    public function bonus()
    {
        return $this->hasOne('App\Models\Bonus');
    }
}
