<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    protected $fillable = ['item_id', 'user_id'];

    /**
     * Get the User for this Wish List.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    /**
     * Get the Items for this Wish List.
     */
    public function items()
    {
        return $this->belongsToMany('App\Models\Item');
    }
}
