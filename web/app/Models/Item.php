<?php

namespace App\Models;

use App\Fee;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['name', 'external_id', 'description', 'composition', 'price', 'amount', 'path', 'categories', 'hit', 'extra'];

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    /**
     * Get the Photo photos for this Items.
     */
    public function photos()
    {
        return $this->hasMany('App\Models\Photo');
    }

    public function getCategoryListsAttribute()
    {
        if ($this->categories()->count() < 1) {
            return null;
        }
        return $this->categories->pluck('id')->all();
    }


    public function carts()
    {
        return $this->hasMany('App\Models\Cart');
    }

    public function wish_lists()
    {
        return $this->hasMany('App\Models\WishList');
    }

}
