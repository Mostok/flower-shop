<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    public $timestamps = false;

    /**
     * Get the Cart for this Order.
     */
    public function orders()
    {
        return $this->hasMany('App\Models\PaymentMethods');
    }
}
