<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Bonus extends Model
{
    const LEVEL_1 = 5;
    const LEVEL_2 = 10;
    const LEVEL_3 = 15;

    /**
     * Get the Order for this Bonus.
     */
    public function order()
    {
        return $this->hasOne('App\Models\Bonus');
    }

    /**
     * Get the User for this Bonus.
     */
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }

    public static function getLevels()
    {
        return [self::LEVEL_1, self::LEVEL_2, self::LEVEL_3];
    }

    public static function setDiscount($bonus)
    {
        $discount = 0;

        if ($bonus >= self::LEVEL_1 && $bonus < self::LEVEL_2) {
            $discount = 3;
        } else if ($bonus >= self::LEVEL_2 && $bonus <= self::LEVEL_3) {
            $discount = 5;
        } else if ($bonus >= self::LEVEL_3) {
            $discount = 7;
        }

        $bonus_exist = Bonus::where('user_id', Auth::user()->id)->first();
        // var_dump($bonus_exist);
        // exit();
        if(is_null($bonus_exist)) {
            $user = User::findOrFail(Auth::user()->id);
            $create_bonus = new Bonus();
            $create_bonus->bonus = $discount;
            // var_dump($create_bonus);exit();
            $user->bonus()->save($create_bonus);

        } else {
            $bonus_exist->bonus = $discount;
            $bonus_exist->save();
        }
    }
}
