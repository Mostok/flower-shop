<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Greeting extends Model
{
    public $timestamps = false;

    protected $fillable = ['text', 'order_id'];

    /**
     * Get the Order for this Greeting.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
