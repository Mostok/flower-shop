<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            // remove relations to products
            $model->items()->detach();
        });
    }

    public function items()
    {
        return $this->belongsToMany('App\Models\Item');
    }

    public function getTotalProductsAttribute()
    {
        return Item::whereIn('id', $this->related_items_id)->count();
    }

}
