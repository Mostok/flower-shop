<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Status;
use App\Models\User;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Get Order list
     *
     * @return \App\Models\Order[]
     */
    function list() {

        $orders = Order::with('delivery')
            ->with('bonus')
            ->with(['cart.items', 'cart.items.photos', 'cart.user', 'cart.promo'])
            ->with('greeting')
            ->with('status')
            ->get();

        return $orders;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $data = $request->all();
// var_dump($request->all());
            // echo Auth::check();
            // exit();
            $cart = Cart::findOrFail($data['cart_id']);
            $sum = $cart->sum();

            $order = Order::create([
                'cart_id' => $data['cart_id'],
                'delivery_id' => isset($data['delivery_id']) ? $data['delivery_id'] : null,
                'price' => $sum,
                'payment_method_id' => null,
                'bonus_id' => isset($data['bonus_id']) ? $data['bonus_id'] : null,
                'status_id' => 1,
            ]);

            // Attach this cart to order
            $attach = Cart::findOrFail($data['cart_id']);
            $attach->order_id = $order->id;
            $attach->save();

            return response()->json(['success' => true, 'order' => $order]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            return response()->json(['message' => 'Successfully create order', 'order' => $order]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            return response()->json(['message' => 'Successfully destroy order', 'id' => $id]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    public function changeStatus(Request $request)
    {
        try {
            $data = $request->all();
            $order = Order::findOrFail($data['order_id']);
            $order->status_id = $data['status_id'];
            $order->save();
            return response()->json(['message' => 'Successfully changed status', 'id' => $order]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    public function statuses()
    {
        return Status::all();
    }

    public function cartAttach($user_id)
    {
        $success = true;

        try {
            $cart = Cart::create([
                'user_id' => (int) $user_id,
            ]);

        } catch (\Illuminate\Database\QueryException $exception) {
            $cart = $exception->errorInfo;
            $success = false;
        };

        return ['success' => $success, 'result' => $cart];
    }

    public function itemsAttach(Request $request)
    {
        $success = true;
        $cart = Cart::findOrFail($request->cart_id);

        try {
            foreach ($request->items as $item) {
                $cart->items()->attach([$item['item_id'] => ['quantity' => $item['quantity']]]);
            }

            $message = "Successfully added Items";

        } catch (\Illuminate\Database\QueryException $exception) {
            $message = $exception->errorInfo;
            $success = false;
        };

        return ['success' => $success, 'message' => $message];
    }

    public function deliveryAttach(Request $request)
    {
        $client = $request->client_id;
        $delivery = $request->delivery;

        $user = User::findOrFail($client);

        $res = $user->deliveries()->create([
            'city' => $delivery['city'],
            'street' => $delivery['street'],
            'building' => $delivery['building'],
            'suite' => $delivery['suite'],
        ]);

        return $res->makeVisible('id');
    }
}
