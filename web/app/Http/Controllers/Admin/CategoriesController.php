<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Get Categories list
     *
     * @return \App\Models\Category[]
     */
    function list() {
        $categories = Category::get();

        return $categories;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $category = new Category();
            $category->name = $request->name;

            $category->save();

            return response()->json(['message' => 'Successfully create category', 'category' => $category]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->name = $request->name;

            $category->update();

            return response()->json(['message' => 'Successfully updated category', 'category' => $category]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $category = Category::find($id);
            $category->delete();

            return response()->json(['message' => 'Successfully destroy item', 'id' => $id]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }
}
