<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Closure;
use App\AdminUser as User;

class AdminController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Get AdminUser list
     *
     * @return \App\Models\AdminUser[]
     */
    function list() {
        $adminUsers = User::get();

        return $adminUsers;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->role = $request->role;

            $user->save();

            return response()->json(['message' => 'Successfully create user', 'user' => $user]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $user->role = $request->role;

            $user->update();

            return response()->json(['message' => 'Successfully create user', 'user' => $user]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = User::find($id);
            $user->delete();

            return response()->json(['message' => 'Successfully destroy user', 'id' => $id]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }
}
