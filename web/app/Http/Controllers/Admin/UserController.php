<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Faker\Factory as Faker;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Get User list
     *
     * @return \App\Models\User[]
     */
    function list() {
        $users = User::get();

        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = true;
        $exist = false;

        try {
            $user = User::where('phone', $request['phone'])->first();
            if (!$user) {
                $request['password'] = Str::random(8);

                $user = User::create([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'phone' => $request['phone'],
                    'password' => Hash::make($request['password']),
                ]);

                Log::info("Generated password for " . $user->phone . " :" . $request['password']);
            } else {
                if ($user->confirmed) {
                    $exist = true;
                }
            }
        } catch (\Illuminate\Database\QueryException $exception) {
            $user = $exception->errorInfo;
            $success = false;
        };

        return ['success' => $success, 'result' => $user, 'exist' => $exist];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $user = User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;

            $user->update();

            return response()->json(['message' => 'Successfully create user', 'user' => $user]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::find($id);
            $user->delete();

            return response()->json(['message' => 'Successfully destroy user', 'id' => $id]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    public function sendSms(Request $request)
    {
        $success = false;
        $faker = Faker::create();
        $code = strtoupper($faker->firstName);
        $user = User::where('phone', $request->phone)->first();
        $user->code = $code;
        $user->save();
        // exit();
        // echo $code;
        return ['success' => true];

        $client = new Client();
        $response = $client->request('POST', "http://api.prostor-sms.ru/messages/v2/send.json", [
            "json" => ["messages" => [
                "phone" => $request->phone,
                "clientId" => 1,
                "text" => "Ваше слово подтверждения: " . $code,
            ],
                "showBillingDetails" => true,
                "login" => "t79044403375",
                "password" => "854492",
            ],
        ]);

        $result = json_decode($response->getBody());
        if ($result->status === 'ok') {
            $success = true;
        }

        return ['success' => $success];
    }

    public function confirm(Request $request)
    {
        $success = true;
        $code = strtoupper($request->code);
        $user = User::where('phone', $request->phone)->first();

        if ($code === $user->code) {
            $user->confirmed = true;
            $user->password = Hash::make($code);
            $user->code = null;
            $user->save();

            Log::info("Password for " . $user->id . " :" . $code);

        } else {
            $success = false;
        }

        return ['success' => $success];
    }

}
