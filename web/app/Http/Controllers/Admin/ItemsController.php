<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ItemsController extends Controller
{
    /**
     * Get Items list
     *
     * @return \App\Models\Item[]
     */
    function list() {
        $items = Item::with('categories')->where('extra', 0)->get();

        return $items;
    }

    /**
     * Get Extra Items
     *
     * @return \App\Models\Item[]
     */

    public function extras()
    {
        $items = Item::with('categories')->where('extra', 1)->get();

        return $items;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $item = new Item();
            $item->name = $request->name;
            $item->composition = $request->composition;
            $item->description = $request->description;
            $item->extra = $request->extra;
            $item->price = $request->price;

            $item->save();
            $item->categories()->sync($request->get('categories'));

            $item = Item::with('categories')->findOrFail($item->id);

            return response()->json(['message' => 'Successfully create item', 'item' => $item]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // var_dump($request->all());exit();
        try {
            $item = Item::findOrFail($id);
            $item->name = $request->name;
            $item->composition = $request->composition;
            $item->description = $request->description;
            $item->price = $request->price;

            $item->update();

            $item->categories()->sync($request->get('categories'));

            $item = Item::with('photos')->with('categories')->findOrFail($item->id);

            return response()->json(['message' => 'Successfully create item', 'item' => $item]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $item = Item::find($id);
            $item->delete();

            return response()->json(['message' => 'Item activity successfully change', 'item_id' => $id]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }

    public function active($id)
    {
        try {
            $item = Item::findOrFail($id);
            $item->active = !$item->active ? true : false;
            $item->save();
            return response()->json(['message' => 'Item activity successfully changed', 'item_id' => $id]);
        } catch (Exception $e) {
            return response()->json(['error' => $e], 401);
        }
    }
}
