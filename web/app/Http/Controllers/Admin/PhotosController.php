<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class PhotosController extends Controller
{

    public function __construct()
    {
        // $this->middleware(['auth', 'admin']);

    }

    protected function savePhoto($photo)
    {
        $file_name = Str::random(20) . '.jpg';
        $photo = base64_encode(file_get_contents($photo));
        $combined = Image::make($photo);

        Storage::disk('public')->put($file_name, $combined->fit(800, 800, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 80));

        return $file_name;
    }

    protected function removePhoto($path)
    {
        $getPath = explode('/', $path);

        if (Storage::disk('public')->exists($getPath[2])) {
            Storage::disk('public')->delete($getPath[2]);
        } else {
            dd("No file found");
        }
    }

    public function show($item_id)
    {
        $photos = Photo::where('item_id', $item_id)->get();

        return $photos;
    }

    public function store(Request $request)
    {
        $success = true;

        $item = Item::findOrFail($request->get('item_id'));
        $loaded = [];

        foreach ($request->photos as $photo) {
            $img = new Photo(['path' => $this->savePhoto($photo['path'])]);
            $loaded[] = $item->photos()->save($img);
        }

        return response(['data' => $loaded, 'success' => $success], 201);
    }

    public function destroy(int $id)
    {
        $photo = Photo::findOrFail($id);
        $this->removePhoto($photo->path);
        $photo->delete();

        return $photo;
    }

}
