<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Bonus;
use App\Models\Cart;
use App\Models\Delivery;
use App\Models\User;
use Faker\Factory as Faker;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function createDelivery(Request $request)
    {
        $data = $request->all();

        if (Auth::check()) {
            $user = User::findOrFail(Auth::user()->id);
        } else {
            $user = User::findOrFail($data['user']);
        }

        $res = $user->deliveries()->create([
            'city' => $data['city'],
            'street' => $data['street'],
            'building' => $data['building'],
            'suite' => $data['suite'],
        ]);

        return $res;
    }

    public function defaultDelivery(Request $request)
    {
        $success = true;

        try {
            $user_deliveries = Delivery::where('user_id', $request->user_id)->get();

            foreach ($user_deliveries as $delivery) {
                $delivery->active = false;
                $delivery->save();
            }

            $default = Delivery::findOrFail($request->delivery_id);
            $default->active = true;
            $result = $default->save();

        } catch (\Illuminate\Database\QueryException $exception) {
            $result = $exception->errorInfo;
            $success = false;
        }

        return ['success' => $success, 'result' => $result];
    }

    public function getBonus($user_id = null)
    {
        if (is_null($user_id) && Auth::check()) {
            $user_id = Auth::user()->id;
        }

        return Bonus::select('bonus')->where('user_id', $user_id)->first();
    }

    public function profile()
    {
        if (Auth::check()) {
            $orders = Cart::with('order')->with('items')->where('user_id', Auth::user()->id)->whereNotNull('order_id')->get();
        }

        $user = User::with('bonus')->with('deliveries')->findOrFail(Auth::user()->id);
        // var_dump($user->deliveries()->get());
        // exit();
        $current_bonus = self::calculateBonus();

        Bonus::setDiscount($current_bonus);

        $level_state = [];

        foreach (Bonus::getLevels() as $level) {
            if ($current_bonus < $level) {
                $level_state = array('current_state' => $current_bonus, 'next_level' => $level);
                $level_state = json_encode($level_state);
                break;
            }
        }

        // echo $level_state;
        // exit();

        return view('cabin', compact('orders', 'user', 'level_state'));
    }

    protected static function calculateBonus()
    {
        $quantity = 0;

        $order_carts = Auth::user()->carts()->whereNotNull('order_id')->with(['items' => function ($q) {
            $q->where('extra', 0);
        }])->get();

        foreach ($order_carts as $cart) {
            foreach ($cart->items as $item) {
                $quantity += $item->pivot->quantity;
            }
        }

        return $quantity;
    }

    public function sendSms(Request $request)
    {
        $success = false;
        $faker = Faker::create();
        $code = strtoupper($faker->firstName);
        $user = User::where('phone', $request->phone)->first();
        $user->code = $code;
        $user->save();
        // exit();

        $client = new Client();
        $response = $client->request('POST', "http://api.prostor-sms.ru/messages/v2/send.json", [
            "json" => ["messages" => [
                "phone" => $request->phone,
                "clientId" => 1,
                "text" => "Ваше слово подтверждения: " . $code,
            ],
                "showBillingDetails" => true,
                "login" => "t79044403375",
                "password" => "854492",
            ],
        ]);

        $result = json_decode($response->getBody());
        if ($result->status === 'ok') {
            $success = true;
        }

        return ['success' => $success];
    }

    public function confirm(Request $request)
    {
        $success = true;
        $code = strtoupper($request->code);
        $user = User::where('phone', $request->phone)->first();

        if ($code === $user->code) {
            $user->confirmed = true;
            $user->code = null;
            $user->password = Hash::make(strtolower($code));
            $user->save();

            Log::info("Password for " . $user->id . " :" . $code);

        } else {
            $success = false;
        }

        return ['success' => $success];
    }
}
