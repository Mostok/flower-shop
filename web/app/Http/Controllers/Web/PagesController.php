<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function about() {
        return view('about');
    }

    public function delivery() {
        return view('delivery');
    }
}
