<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File as File;
use Illuminate\Support\Facades\Storage;

class ItemsController extends Controller
{

    public function __construct()
    {
        // $this->middleware(['auth', 'admin']);
    }

    /**
     * Get Items list
     *
     * @return \App\Models\Item[]
     */
    function list() {
        $items = Item::with('photos')->with('categories')
            ->where('extra', 0)
            ->where('active', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        $recommends = Item::with('photos')
            ->where('extra', 1)
            ->where('price', '>', 0)
            // ->where('active', 1)
            ->get();
            // ->random(3);

        return view('catalogue', compact('items', 'recommends'));
    }

    /**
     * Get Items list by Extra option
     *
     * @return \App\Models\Item[]
     */
    public function gifts()
    {
        $greeting_category = "Открытка";

        $items = Item::with('photos')->whereHas('categories', function ($q) use ($greeting_category) {
                $q->where('categories.name', 'NOT LIKE', $greeting_category);
            })
            ->where('price', '>', 0)
            ->where('extra', 1)
            // ->where('active', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('gifts', compact('items'));
    }

    /**
     * Get Items list by Extra option
     *
     * @return \App\Models\Item[]
     */
    public function greetings()
    {
        $greeting_category = "Открытка";

        $items = Item::with('photos')->whereHas('categories', function ($q) use ($greeting_category) {
                $q->where('categories.name', 'LIKE', $greeting_category);
            })
            ->where('price', '>', 0)
            ->where('extra', 1)
            ->where('active', 1)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('greeting-cards', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:items',
            'description' => 'required',
            'price' => 'required',
            // 'path' => 'mimes:jpeg,png|max:10240',
        ]);
        $data = $request->only('name', 'description', 'external_id', 'composition', 'price', 'amount', 'hit', 'extra');
        // var_dump($data);
        // exit();

        $item = Item::create($data);
        $item->categories()->sync($request->get('categories'));

        return $item;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $item = Item::with('photos')->where('id', $id)->first();
        $item_categories = $item->categories->toArray();

        // Get all items with this categories exclude current Item
        $recommends = Item::whereNotIn('id', [$item->id])->whereHas('categories', function ($q) use ($item_categories) {
            $q->whereIn('categories.id', $item_categories);
        })->get();

        if (count($recommends) > 3) {
            $recommends = $recommends->random(3);
        }

        // echo '<pre>';
        // var_dump($recommend->get());
        // exit();

        return view('details', compact('item', 'recommends'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::findOrFail($id);

        return view('admin.items.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::findOrFail($id);

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
            if ($item->photo !== '') {
                $this->deletePhoto($item->photo);
            }

            $item->update($data);
            return redirect()->route('admin.items.edit', $id);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'price' => 'required',
                'photo' => 'mimes:jpeg,png|max:10240',
                'amount' => 'numeric|min:1',
            ]);

            $data = $request->only('name', 'external_id', 'composition', 'description', 'size', 'price', 'amount', 'hit', 'extra');

            if (count($request->get('categories')) > 0) {
                $item->categories()->sync($request->get('categories'));
            } else {
                // no category set, detach all
                $item->categories()->detach();
            }

            $item->update($data);
            return redirect()->action('ItemsController@index');
        }
    }

    public function filter(Request $request): Collection
    {
        $filters = $request->all();

        $categories = self::gatherCategories($filters);

        $items = new Item();
        $items = $items->newQuery();

        foreach ($filters as $filter) {
            // Filters for Price
            if ($filter['type'] === 'price') {

                switch ($filter['value']) {
                    case $filter['value'] == 1000:{
                            $items->where('price', '<=', $filter['value'])->where('price', '>', 0);
                            break;
                        }
                    case $filter['value'] == 1500:{
                            $items->whereBetween('price', [1000, $filter['value']]);
                            break;
                        }
                    case $filter['value'] == 2500:{
                            $items->whereBetween('price', [1500, $filter['value']]);
                            break;
                        }
                    case $filter['value'] == 3500:{
                            $items->where('price', '>=', 2500);
                            break;
                        }
                }
            }

            if ($filter['type'] === 'hit') {
                $items->where('hit', true);
            }

            if ($filter['type'] === 'extra') {
                $items->where('extra', true);
            }

            if (count($categories) > 0) {
                $items = Item::whereHas('categories', function ($q) use ($categories) {
                    $q->whereIn('categories.id', $categories);
                });
            }
        }
        // echo count($items->get()) . " COUNT";

        return $items->with('photos')->with('categories')->where('extra', 0)->get();
    }

    protected function gatherCategories($filters): array
    {
        $categories = [];

        foreach ($filters as $filter) {
            if ($filter['type'] === 'category') {
                $categories[] = $filter['id'];
            }
        }

        return $categories;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id): Collection
    {
        $item = Item::find($id);
        $item->delete();

        return Item::get();
    }

    /**
     * Get Categories list
     *
     * @return \App\Models\Category[]
     */
    public function getCategories()
    {
        $categories = Category::get();

        return $categories;
    }
}
