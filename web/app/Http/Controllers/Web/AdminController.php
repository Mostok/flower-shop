<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Show the application admin. (NEW)
     *
     * @param  Request  $request
     * @return Application|Factory|View
     */
    public function admin(Request $request)
    {
        return view('admin');
    }
}
