<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['string', 'min:13', 'unique:users'],
            'email' => ['string'],
            'password' => ['string', 'min:8'],
        ]);
    }

    protected function create(array $data)
    {
        $password = Str::random(8);

        $user = User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($password),
        ]);

        Log::info("Temp password for " . $user->id . " :" . $password);

        return $user;
    }

    public function register(Request $data)
    {
        $success = true;
        $exist = false;

        // $cart = Cart::where('user_id', $data['temp'])->first();

        // print_r($cart);
        // exit();
        try {
            $user = User::where('phone', $data->phone)->first();
            if (!$user) {
                // print_r($data);exit();

                $user = $this->create($data->toArray());
            } else {
                if ($user->confirmed) {
                    $exist = true;
                }
            }

            $cart = Cart::findOrFail($data['cart']);

            if ($cart) {
                $cart->user_id = $user->id;
                $cart->save();
            }

        } catch (\Illuminate\Database\QueryException $exception) {
            $user = $exception->errorInfo;
            $success = false;
        };

        return ['success' => $success, 'result' => $user, 'exist' => $exist];
    }
}
