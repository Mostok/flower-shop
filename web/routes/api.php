<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// API routes for AdminTable

Route::post('auth/login', 'Admin\AuthController@login');

Route::group(['namespace' => 'Admin', 'middleware' => ['auth:jwt']], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('logout', 'AuthController@logout');
        Route::get('refresh', 'AuthController@refresh');
        Route::get('user', 'AuthController@me');
    });

    Route::group(['prefix' => 'users'], function () {
        Route::group(['prefix' => 'web'], function () {
            Route::get('/list', 'UserController@list');
            Route::get('/{id}/delete', 'UserController@destroy');
            Route::post('/store', 'UserController@store');
            Route::post('/{id}/update', 'UserController@update');
            Route::post('confirmation', 'UserController@sendSms');
            Route::post('confirmation/try', 'UserController@confirm');
        });

        Route::group(['prefix' => 'admin'], function () {
            Route::get('/list', 'AdminController@list');
            Route::delete('/{id}', 'AdminController@destroy');
            Route::post('/store', 'AdminController@store');
            Route::post('/{id}/update', 'AdminController@update');
        });
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('/list', 'CategoriesController@list');
        Route::delete('/{id}', 'CategoriesController@destroy');
        Route::post('/store', 'CategoriesController@store');
        Route::post('/{id}/update', 'CategoriesController@update');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/list', 'OrdersController@list');
        Route::delete('/{id}', 'OrdersController@destroy');
        Route::post('/create', 'OrdersController@store');
        Route::post('/{id}/update', 'OrdersController@update');
        Route::post('/change/status', 'OrdersController@changeStatus');
        Route::get('/cart/{user_id}', 'OrdersController@cartAttach');
        Route::post('/delivery', 'OrdersController@deliveryAttach');
        Route::post('/items', 'OrdersController@itemsAttach');

    });

    Route::group(['prefix' => 'items'], function () {
        Route::get('/list', 'ItemsController@list');
        Route::get('/list/extras', 'ItemsController@extras');
        Route::get('/{id}/active', 'ItemsController@active');
        Route::delete('/{id}', 'ItemsController@destroy');
        Route::post('/store', 'ItemsController@store');
        Route::post('/{id}/update', 'ItemsController@update');
    });

    Route::group(['prefix' => 'photos'], function () {
        Route::get('/item/{id}', 'PhotosController@show');
        Route::post('store', 'PhotosController@store');
        Route::delete('{id}', 'PhotosController@destroy');
    });

    Route::get('/statuses', 'OrdersController@statuses');

    Route::group(['prefix' => 'promo'], function () {
        Route::get('/list', 'PromoController@list');
        // Route::delete('/{id}', 'PromoController@destroy');
        Route::post('store', 'PromoController@store');
        // Route::post('/{id}/update', 'CategoriesController@update');
    });

});
