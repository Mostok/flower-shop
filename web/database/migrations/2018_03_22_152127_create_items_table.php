<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->mediumInteger('external_id')->nullable();
            $table->mediumText('description');
            $table->mediumText('composition')->nullable();
            $table->integer('price');
            $table->boolean('hit')->default(false);
            $table->boolean('extra')->default(false);
            $table->boolean('active')->default(false);
            $table->integer('admin_id')->unsigned()->default(1);
            $table->timestamps();
        });

        Schema::table('items', function ($table) {
            $table->foreign('admin_id')->references('id')->on('admin_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('items');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
