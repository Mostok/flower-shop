<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('city');
            $table->string('street');
            $table->string('building');
            $table->integer('suite')->nullable();
            $table->string('name')->nullable();
            // Active - Выбранный по умолчанию
            $table->boolean('active')->default(true);
        });

        Schema::table('deliveries', function ($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('deliveries');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
