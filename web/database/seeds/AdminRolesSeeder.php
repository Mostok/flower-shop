<?php

use Illuminate\Database\Seeder;

class AdminRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'superadmin',
            'admin'
        ];

        foreach ($roles as $role) {
            DB::table('admin_roles')->insert([
                'role' => $role
            ]);
        }
    }
}
