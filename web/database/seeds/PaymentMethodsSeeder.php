<?php

use App\Models\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $payment_methods = [
            'Перевод на карту после подтверждения заказа',
            'Наличными курьеру',
        ];

        foreach ($payment_methods as $method) {
            PaymentMethod::create([
                'payment_method' => $method,
            ]);
        }
    }
}
