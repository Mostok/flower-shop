<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminRolesSeeder::class);
        $this->call(AdminUserSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(ItemsSeeder::class);
        $this->call(PaymentMethodsSeeder::class);
        $this->call(PromoSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(StatusesSeeder::class);
    }
}
