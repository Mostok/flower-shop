<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Offline',
            'email' => null,
            'phone' => null,
            'password' => null,
            'confirmed' => true,
        ]);
        User::create([
            'name' => 'Vkontakte',
            'email' => null,
            'phone' => null,
            'password' => null,
            'confirmed' => true,
        ]);
        User::create([
            'name' => 'Instagram',
            'email' => null,
            'phone' => null,
            'password' => null,
            'confirmed' => true,
        ]);

    }
}
