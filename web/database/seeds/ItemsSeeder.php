<?php

use App\Models\Item;
use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            0 => [
                'Букет Весенний',
                'Замечательный букет',
                1000,
                1,
                0,
                '3uHvPSDdiHmxf9Xxryw6iFQQJR3iNwtQdPp1gD2a.jpeg',
                1,
            ],

            1 => [
                'Букет Осенний',
                'Замечательный букет',
                1200,
                1,
                0,
                'wU6T54pxoAtBeQBkZdwyzlWHi89qImw7gDzg6rb3.jpeg',
                1,

            ],

            2 => [
                'Супербукет',
                'Замечательный букет',
                1500,
                0,
                0,
                'ZomHCH4ihBCgD5kMoaJCarTRTzlNdAb5bxiTq0oH.jpeg',
                1,
            ],

            3 => [
                'Монобукет из роз',
                'Замечательный букет',
                2000,
                0,
                0,
                '3uHvPSDdiHmxf9Xxryw6iFQQJR3iNwtQdPp1gD2a.jpeg',
                1,
            ],

            4 => [
                'Открытка',
                'С Вашим пожеланием в подарок',
                0,
                0,
                1,
                'ZomHCH4ihBCgD5kMoaJCarTRTzlNdAb5bxiTq0oH.jpeg',
                1,
            ],
        ];

        $i = 0;

        foreach ($items as $item) {
            $new = Item::create([
                'name' => $item[0],
                'description' => $item[1],
                'price' => $item[2],
                'hit' => $item[3],
                'extra' => $item[4],
                'admin_id' => $item[6],
                'active' => true
            ]);

            $new->photos()->create(['path' => $item[5]]);

            if ($i < count($items) - 1) {
                $new->categories()->sync(rand(1, 5));
            } else {
                $new->categories()->sync(6);
            }

            $i++;
        }
    }
}
